import javafx.scene.shape.*;
import javafx.application.Application;
import javafx.event.*;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.scene.paint.Color;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.animation.Animation;
import javafx.animation.AnimationTimer;
import javafx.animation.Interpolator;
import javafx.animation.PathTransition;
import javafx.animation.PathTransition.OrientationType;
import javafx.animation.Timeline;
import javafx.animation.Transition;
import javafx.stage.Stage;
import javafx.util.Duration;
import java.util.*;

public class Life extends Application{

    private static final int x_size = 80;
    private static final int y_size = 80;
    private static final int alivePrs = 5;
    private static int life[][] = new int[y_size][x_size];
    private long moment = System.nanoTime();

      
        public static void main(String[] args) {
            launch(args);
        }
        
        @Override
        public void start(Stage primaryStage) {
            primaryStage.setTitle("Life");
            Group root = new Group();
            
            Rectangle cells[][] = new Rectangle[y_size][x_size];
            for (int i = 0; i < y_size; i++){
                for (int j = 0; j < x_size; j++){
                    cells[i][j]= new Rectangle(10,10,Color.WHITE);
                    cells[i][j].setX(j*10);
                    cells[i][j].setY(i*10);
                    root.getChildren().add(cells[i][j]);
                }
            }
            
            setup();
                   
            AnimationTimer anim = new AnimationTimer(){
            
                @Override
                public void handle(long now) {
                        if ((now-moment)>28_000_000){
                        checkLife();
                        for (int i = 0; i < y_size; i++){
                            for (int j = 0; j < x_size; j++){
                                if (life[i][j]==0)
                                    cells[i][j].setFill(Color.WHITE);
                                    else
                                    cells[i][j].setFill(Color.BLACK);
                            }
                        }
                        moment = now;
                    }
                }
            };

            primaryStage.setScene(new Scene(root, 10*x_size, 10*y_size));
            anim.start();
            primaryStage.show();
        }
        
           private static void checkLife(){
        for (int i = 0; i < y_size; i++){
            for (int j = 0; j < x_size; j++){
                int aliveNeigh = aliveNeighCount(i, j);
                if (aliveNeigh==3)
                    life[i][j]=1;
                if ((aliveNeigh>4)||(aliveNeigh<2))
                    life[i][j]=0;
            }
        }
    }

    private static int aliveNeighCount(int i, int j){
        int aliveNeigh = 0;
        if ((i>0) && (life[i-1][j]==1))
            aliveNeigh++;
        if ((i<(y_size-1)) && (life[i+1][j]==1))
            aliveNeigh++;
        if ((j>0) && (life[i][j-1]==1))
            aliveNeigh++;
        if ((j<(x_size-1)) && (life[i][j+1]==1))
            aliveNeigh++;
        if ((i>0) && (j>0) && (life[i-1][j-1]==1))
            aliveNeigh++;
        if ((i>0) && (j<(x_size-1)) && (life[i-1][j+1]==1))
            aliveNeigh++;
        if ((i<(y_size-1)) && (j>0) && (life[i+1][j-1]==1))
            aliveNeigh++;
        if ((i<(y_size-1)) && (j<(x_size-1)) && (life[i+1][j+1]==1))
            aliveNeigh++;
        return aliveNeigh;
    }
    
    private void setup(){
         Random rand = new Random();
        for (int i = 0; i < y_size; i++){
            for (int j = 0; j < x_size; j++){
                if (rand.nextInt(100)>(alivePrs+1))
                    life[i][j] = 0;
                else life[i][j] = 1;
            }
        }
    }
 
}
